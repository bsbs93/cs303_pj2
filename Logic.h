#ifndef _LOGIC_H
#define _LOGIC_H
// the logic delegate only has access to the basic operations of the elevator. Move up, move down, and stop. 
// It also handles the calls and destination floors requested using the elevator public functions.

void sleepp(unsigned int mseconds); // found this function on stackoverflow somewhere
call randCall();

class Logic // class for running elevator
{
public:
	Elevator Elv; 
	Logic(Elevator& E);
	void service(); // main function for elevator logic 
	int emptyLoop();//loop that runs when empty, adds floor calls to deque as a batch
	void processDir(direction D = STOP);// process items from deque (in given direction) and add to requested floors set, default is front of deque
	void processSet(); //go to the floors in the current set
	void goTo(int floor);// go to specified floor
};
Logic::Logic(Elevator& E)
{
	Elevator& Elv = E;
}

void Logic::service() 
{
	while (true)
	{
		if (Elv.requested.empty() && Elv.callDeque.empty())
		{
			if (mode == 0) // 0 == manual
			{
				int c = emptyLoop();
				if (c == 0)
					return; // exit service loop
			}
			else if (mode == 1) // 1 == simulation
			{
				cout << "0 to exit, 1 to continue\n";
				char x;
				std::cin >> x;
				if (x == '0')
				{
					cout << "\n\n ------ Simulation END ------\n\n Total run time: " << Elv.getTime() << " seconds\n\n";
					return;
				}
				int randCalls = 1 + (rand() % 5);
				cout << '\n'<< randCalls;
				while (randCalls != 0)
				{
					call j = randCall();
					Elv.floorCall(j);
					randCalls--;
				}
				Elv.setDirection(Elv.callDeque.front().dir);
				cout << " calls to be added:\n";
				for (size_t i = 0; i < Elv.callDeque.size(); i++)
				{
					Elv.callDeque[i].displayCall();
				}
			}
		}
			
			processDir(Elv.getDirection());
			processSet();
		}
	}

int Logic::emptyLoop()
{
	cout << "\n...All calls currently serviced...\n ";
	while (true) // continue adding floor calls to batch until service start
	{
		cin.ignore(1, '\n');
		cout << "\nEnter UP or DOWN:\n(Enter 0 to exit)\n"; // get direction
		string direc;
		getline(cin, direc);
		if (direc == "0") return 0;
		int f;
		cout << "\nEnter current floor position:\n";
		cin >> f;	// get current floor user is calling from
		while (f<BOTTOM || (f>TOP)) // error checking for floor numbers
		{
			cout << "\nInvalid Entry!\n Enter current floor: \n";
			cin >> f;
		}
		if ((direc == "UP") || (direc == "up") || (direc == "u")) // if direction called is UP, add to floor call deque
		{
			Elv.floorCall(call(UP, f));
		}
		else if ((direc == "DOWN") || (direc == "down") || (direc == "d")) // or direction may be DOWN, still add to floor call deque
		{
			Elv.floorCall(call(DOWN, f));
		}
		else { // else there must be an error with the direction or floor, nothing added to call deque
			cout << "Error, floor not added to call list! invalid direction or floor\n";
		}
		cout << "Add another call (Y)es? or (N) to begin service\n";
		char g;
		cin >> g;
		while (toupper(g) != 'Y' && toupper(g) != 'N')
		{
			cout << "\nEnter y or n\n";
			std::cin >> g;
		}
		if (toupper(g) == 'N')
		{
			Elv.setDirection(Elv.callDeque.front().dir);
			return 1; // if user enters 'n', stop batching calls, and begin servicing
		}
	}
}
void Logic::processSet() // floors in the set are floors we need to make stops at
{
	while (true)
	{
		if (Elv.requested.empty())
		{
			Elv.switchDirection();
			return; // if all floors processed, exit function
		}
		else
		{
			if (Elv.getDirection() == UP)
			{
				int destFloor;
				set<int>::iterator it = Elv.requested.begin();
				destFloor = *it;
				goTo(destFloor);
				Elv.requested.erase(it);
			}
			else if (Elv.getDirection() == DOWN)
			{
				int destFloor;
				set<int>::iterator iter = Elv.requested.end(); //iterate the set in reverse for down (start w/ highest floor first)
				--iter;
				destFloor = *iter;
				goTo(destFloor);
				Elv.requested.erase(iter);
			}
		}
	}
}
void Logic::processDir(direction D) // add all calls in the given direction to the req. floor set
{
	// set the current working direction to match the call of the first deque item (First called, first served)
	if (D == 1)
		cout << "Now servicing \31\n";  // we are now servicing all calls in deque in either the up or down direction (whichever called first)
	else if (D == 2)
		cout << "Now servicing \30\n";
	deque<call>::iterator iter = Elv.callDeque.begin();
	// process items from deque into set
	while (iter != Elv.callDeque.end()) // for each item in the call deque..
	{
		if (iter->dir == D)  // if that call is in the same direction we will pick them up
		{
			Elv.requested.insert(iter->atFloor); // add the corresponding floor to the pickup set
			iter = Elv.callDeque.erase(iter); // remove call from the deque, it has been handled
		}
		else iter++;
	}
}
void Logic::goTo(int floor)
{
	int moves = (Elv.getFloor() - floor);
	if (moves == 0) // at the destination floor already
	{
		sleepp(1200);
		Elv.stop(); // open doors
		return;
	}
	else if (moves < 0) // we are under the dest floor
	{
		while (moves < 0)
		{
			sleepp(1200);
			Elv.moveUp(); // go up
			moves++;
		}
		Elv.stop();
		return;
	}
	else if (moves > 0)
	{
		while (moves > 0) //we are above the target floor
		{
			sleepp(1200);
			Elv.moveDown(); // go down
			moves--;
		}
		Elv.stop();
		return;
	}
	else return;

}

#endif 