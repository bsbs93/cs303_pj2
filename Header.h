#ifndef _HEADER_H
#define _HEADER_H

#include <iostream>
#include <set>
#include<deque>
#include<string>
#include<sstream>
#include<cmath>
#include<ctime>
#include<time.h>
#include <stdlib.h> 
#include"Elevator.h"
#include"Logic.h"


int mode; // 0 manual, 1 simulation

using namespace std;


void sleepp(unsigned int mseconds) // found this function on stackoverflow somewhere
{
	clock_t goal = mseconds + clock();
	while (goal > clock());
}
call randCall()  // returns a randomly generator call
{
	int floor = rand()%TOP + BOTTOM; //random floor between BOTTOM and TOP
	int d = rand()%500+1; // some random number, check if even/odd for up or down
	if (d%2 == 0)
	{
		call toReturn(DOWN, floor);
		return toReturn;
	}
	else if (d%2 == 1)
	{
		call toReturn(UP, floor);
		return toReturn;
	}
}
void elvMenu(Logic& unit) // main menu loop
{
	cout << "WELCOME TO THE ELEVATOR PROGRAM\n-------------------------------\n";
	while (true)
	{
		cout << "What would you like to do? \n1)Manual Mode \n2)Simulation \n3)Reset Elevator \n4)Exit\n";

		int choice;
		cin >> choice;

		switch (choice)
		{
		case 1:
		{
			mode = 0; // sets global mode variable to manual
			unit.service();
			break;
		}
		case 2:
		{
			mode = 1; // set mode to simulation
			unit.service();
			break;
		}
		case 3:
		{
			cout << "\n This will set Elevator back to floor 1 and reset time counter\n\n";
			unit.Elv.clearTime();
			unit.Elv.resetFloor();
			break;
		}
		case 4:
		{
			exit(0);
			break;
		}
		default:
		{
			cout << "\nError try again!\n";
			break;
		}
		}
	}
}
#endif 
