#ifndef _ELEVATOR_
#define _ELEVATOR_
extern int mode;
void sleepp(unsigned int mseconds); // found this function on stackoverflow somewhere

const int TOP = 15;
const int BOTTOM = 1;

using namespace std;

enum direction { STOP, DOWN, UP }; // enum type for direction

struct call  // to hold floor calls, takes direction, and floor called from
{
	direction dir; // (UP/DOWN)
	int atFloor; // (floor user is on)
	call(direction D, int floor);  // constructor for call
	void displayCall(); // display the call
};
call::call(direction D, int floor)
{
	dir = D;
	atFloor = floor;
}
void call::displayCall()
{
	string D;
	switch (dir) // use switch to map enums to strings
	{
	case 1:
		D = "DOWN";
		break;
	case 2:
		D = "UP";
		break;
	default:
		D = "error";
		break;
	}

	cout << D << "  " << atFloor << "\n";
}
/*=========================================================================================
Elevator class, all elevator functions*/
class Elevator
{
	int seconds = 0; // for use in simulation, holds service time
	direction current = STOP;
	int currentFloor = 1;
	
	public:
	deque <call> callDeque; // deque of directions and floors called
	set<int> requested;  //requested floors (doors must open at these floors) --> will contain various floors from callDeque and selections from the inside panel

	Elevator(); // constructor 

	// Elevator Functions (movement and buttons)
	void floorCall(call C); // adds users call request and direction to the call deque.
	void select(); // let users select floors (takes requested floors (inside panel) 
	void moveUp(); // move up a floor
	void moveDown(); // move down a floor
	void stop(); // stop and open doors
	
	// Setters
	void setDirection(direction D); // set direction 
	void switchDirection(); //switches direction
	void clearTime(); // set time back to 0
	void resetFloor(); // set floor back to 1

	// Other getters
	int getTime(); // returns time elapsed
	direction getDirection(); // get current direction
	int getFloor(); // get current floor

					// Helpful debugging functions
	void displayCalls(); // show everything in the call deque
	void displaySet();  // show everything in the requested floor set

};
Elevator::Elevator()
{

	int seconds = 0;
	direction current = STOP; // current moving direction
	int currentfloor = 1; // current floor, default is 1
	deque <call> callDeque; // deque of calls
	set<int> requested; //requested floors
}
void Elevator::floorCall(call C) // adds call to call deque
{
	callDeque.push_back(C); // pushed in deque, FIFO order maintained
}
void Elevator::select() //this is the inside panel of the elevator, where users select their destination floors after stepping inside the cart
{
	cin.ignore(1, '\n');
	cout << "\n*This simulates the elevators inside panel selection* \n";
	string FLOORS; // get all floors in single string
	string token; // token for parsing string above in stream
	
	if (mode == 0) // manual mode
	{
		cout << "\nEnter the destination floors, separated by spaces\n(Enter to skip)\n\n";
		getline(cin, FLOORS); // get floors manually from user
	}
	
	else if (mode == 1) // simulation mode
	{
		int numFloors = rand() % int((TOP + 4)/4); //random number of dest floors to be selected  (up to 2 selections inside, per stop)
		//cout << numFloors;
		FLOORS = "";
		int temprand;
		while (numFloors != 0) // generate random legal floors between 0 and 2 times
		{
			if (current == UP)
			{
				if (currentFloor == TOP) break;
				temprand = rand() % ((TOP + 1) - currentFloor) + currentFloor;
				cout <<"\n  >>> " <<temprand << " was pressed <<<\n";
				sleepp(700);
				FLOORS += (to_string(temprand) + " ");
				numFloors--;
			}
			else if (current == DOWN)
			{
				if (currentFloor == BOTTOM) break;
				temprand = (rand()% currentFloor) + 1; //random floor between BOTTOM and current (going down)
				cout << "\n  >>> " << temprand << " was pressed <<<\n";
				sleepp(700);
				FLOORS += (to_string(temprand) + " ");
				numFloors--;
			}

		}
	}
	istringstream stream(FLOORS); // create a string stream from the requested floors
	if (current == DOWN) // make sure only valid inputs are added to requested floor set
	{
		while(stream >> token)
		{
			if (stoi(token) <= currentFloor && (stoi(token) >= BOTTOM)) // if going down, legal floors are less/=  current floor
				requested.insert(stoi(token)); //convert floor string to int
		}
	}
	else if (current == UP)
	{
		while (stream >> token)
		{
			if (stoi(token) >= currentFloor && (stoi(token) <= TOP)) // when going up, legal floor choices are greater/=  current floor
				requested.insert(stoi(token));
		}
	}

}
void Elevator::moveUp()
{
	if (currentFloor == TOP) //already at top floor, return
	{
		cout << "\n.."<<TOP<<"..\n";
		return;
	}
	else // else move current floor up one, change direction status
	{
		seconds += 10; // we will say it takes 10 seconds to move from one floor to another
		currentFloor++;
		cout << "\n..\30 " << currentFloor << " \30..\n";
		return;
	}
}
void Elevator::moveDown()
{
	if (currentFloor == BOTTOM) // already at bottom floor, can't go lower
	{
		cout << "\n..1..\n";
		return;
	}
	else
	{
		currentFloor -= 1;
		seconds += 12;
		cout << "\n..\31 " << currentFloor << " \31..\n";
		return;
	}
}
void Elevator::stop() // we only stop to open our doors (let people in or out); we may stay stopped after but the initial stop is from doors opening.
{
	cout << "\n..\21 " << currentFloor << " \20..\n";
	sleepp(900); 
	cout << "(..Enter to open doors..)\n";
	seconds += 4; // we will say it takes 4 seconds to open/close the doors
	select();
	seconds += 4;
	cout << "\n..\20 " << currentFloor << " \21..\n";
	sleepp(900); 
	cout << "(..Enter to close doors..)\n";
	return;
}
void Elevator::setDirection(direction D)
{
	current = D;
}
void Elevator::switchDirection() // simply switches elevators moving direction
{
	if (current == UP)
	{
		current = DOWN;
		return;
	}
	else if (current == DOWN)
	{
		current = UP;
		return;
	}
	return;
}
void Elevator::clearTime()
{
	seconds = 0;
	return;
}
void Elevator::resetFloor() 
{
	currentFloor = 1;
	return;
}
int Elevator::getTime()
{
	return seconds;
}
direction Elevator::getDirection()
{
	return current;
}
int Elevator::getFloor()
{
	return currentFloor;
}
void Elevator::displayCalls()
{
	if (callDeque.empty())
	{
		cout << "\nEmpty Call List\n";
		return;
	}
	else
	{
		cout << "\nCalls in call list: \n";
		for (size_t i = 0; i < callDeque.size(); i++)
		{
			callDeque[i].displayCall(); //deque allows access by iterator
		}
	}
}
void Elevator::displaySet()
{
	{
		if (requested.empty())
		{
			cout << "\nEmpty Requested Floor List\n";
			return;
		}
		else
		{
			cout << "\nFloors in selected floor list: \n";
			set<int>::iterator iter = requested.begin(); // no access via index, iterator used instead
			while (iter != requested.end())
			{
				cout << *iter << ", ";
				iter++;
			}
		}
	}
}
#endif